# Dive
Download applications and video games.  Designed to work on MacOS, Windows, Linux, Android, and more (even on the Web)!

## Download
TODO

## API
The standard way to interface is by piping stdout from your program into dive.  Printing text will work the same way as it normally would.  For graphics, use escape codes.  Supports ANSI escape codes as well.  List of escape codes:

```
I0 # 
I1 # 
```
